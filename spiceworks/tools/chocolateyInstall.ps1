﻿$packageArgs = @{
  packageName   = 'spiceworks'
  unzipLocation = "$(Split-Path -parent $MyInvocation.MyCommand.Definition)"
  fileType      = 'exe'
  url           = 'https://download.spiceworks.com/Spiceworks/current/Spiceworks.exe'
  silentArgs    = '/S'
  validExitCodes= @(0,3010)
  softwareName  = 'spiceworks*'
  checksum      = '7E2A38689EF2B99BA3BDD70D2BC8F50D1FE7199B'
  checksumType  = 'sha1'
}

Install-ChocolateyPackage @packageArgs